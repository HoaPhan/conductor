# SOURCE
https://netflix.github.io/conductor/

#BUILD
./gradlew build

#Config
conductor.jetty.server.enabled=true
conductor.grpc.server.enabled=false
db=mysql
jdbc.url=jdbc:mysql://localhost:3306/conductor?useSSL=false
jdbc.username=conductor
jdbc.password=conductor
conductor.mysql.connection.pool.size.max=10
loadSample=false
workflow.external.payload.storage=S3
conductor.mysql.connection.pool.size.max=10
conductor.mysql.connection.pool.idle.min=10
workflow.elasticsearch.instanceType=MEMORY
workflow.elasticsearch.instanceType=EXTERNAL
workflow.elasticsearch.url=http://127.0.0.1:9200

#Docker companions
https://github.com/s50600822/conductor-cheat/blob/master/docker-compose.yaml

# start
java -Dreactor.netty.http.server.accessLogEnabled=true -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=5005 -jar ./server/build/libs/conductor-server-2.15.0-SNAPSHOT-all.jar conductor-config.properties log4j.properties